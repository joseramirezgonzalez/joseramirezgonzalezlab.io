---
title: Home
name: José Hermenegildo Ramírez González
date: 2017-02-20T15:26:23-06:00

categories: [one]
tags: [two,three,four]
interests: [
    "Branching Processes",
    "SPDEs",
    "Spatio-Temporal Modelling",
    "Population dynamics",
    "Topological Data Analysis"
  ]
applications: [
    "Environmental Science",
    "Telemetry data",
    "Extreme Events",
    "Sensor Networks"
  ]
education:
  first:
    course: PhD in Statistics and Probability
    university: "[Centro de Investigación en Matemáticas](https://www.cimat.mx)"
    years: 2018 - 2023
  second:
    course: MSc in Statistics and Probability
    university: "[Centro de Investigación en Matemáticas](https://www.cimat.mx)"
    years: 2016 - 2017
  third:
    course: BSc in Math
    university: "[Universidad de Guanajuato](http://www.demat.ugto.mx)"
    years: 2011 - 2016
---


I am researcher apply Stochastic Processes. My research focusses on developing
spatio-temporal statistical methods to model environmental data. I use open
source software to implement my work such as Julia, R and Python. Check my
[GitHub](https://github.com/joseramirezgonzalez) and [GitLab](https://gitlab.com/joseramirezgonzalez)
accounts to know more about my current work.
![](/images/particles4.png)
