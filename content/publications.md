---
title: Publications and preprints
---

- Kolkovska, E. T., Mimbela, J. A. L., & **Ramírez González, J. H.** (2021). Existence of global solutions of a nonautonomous semilinear equation with varying reaction. *Stochastic Models*, 39(1), 219-231. [https://doi.org/10.1080/15326349.2021.2005628](https://doi.org/10.1080/15326349.2021.2005628)
- López-Mimbela, J., Murillo-Salas, A., and **Ramírez-González, J.** (2024). Occupation time fluctuations of an age-
dependent critical binary branching particle system. ALEA, Lat. Am. J. Probab. Math. Stat., 21. [https://doi.org/10.30757/ALEA.v21-24](https://doi.org/10.30757/ALEA.v21-24)
- **Ramírez-González, J. H.** and Sun, Y. (2023). Integral fractional Ornstein-Uhlenbeck process model for animal movement. [https://arxiv.org/abs/2312.09900](https://arxiv.org/abs/2312.09900)
- **Ramírez-González, J. H.**, Chacón-Montalván, E. A., Moraga, P., and Sun, Y. (2024). Multi-dimensional integral
fractional Ornstein-Uhlenbeck process model for animal movement. [https://doi.org/10.13140/RG.2.2.21463.07841](https://doi.org/10.13140/RG.2.2.21463.07841)
- Murillo-Salas, A.,**Ramírez-González, J. H.**, and Sun, Y. (2024). On a new family of weighted gaussian processes: an application to bat telemetry data. [https://doi.org/10.48550/arXiv.2405.19903](https://doi.org/10.48550/arXiv.2405.19903)
- **Ramírez-González, J. H.** and Sun, Y. (2024). Weighted Sub-fractional Brownian Motion Process: Properties and Generalizations. [https://arxiv.org/abs/2409.04798](https://arxiv.org/abs/2409.04798)
- Kolkovska, E.T. and López-Mimbela, J. and **Ramírez-González, J. H.** (2024). Convergence of a Critical Multitype Bellman-Harris Process.[https://arxiv.org/pdf/2410.17369](https://arxiv.org/pdf/2410.17369)

![](/images/particles.png) ![](/images/particles3.png)![](/images/traB.jpeg)
